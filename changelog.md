0.0.9

* Use aeson-1.0.
* Add nix build.

0.0.8

* No significant changes.

0.0.7

* Update after bug relating to ConstrainedClassMethods in `stratux-types`.

0.0.4

* Tighten dependencies.

0.0.3

* Initial.
