{ mkDerivation, aeson, base, network, stdenv, stratux-types, text
, transformers, websockets
}:
mkDerivation {
  pname = "stratux-websockets";
  version = "0.0.12";
  src = ./.;
  libraryHaskellDepends = [
    aeson base network stratux-types text transformers websockets
  ];
  homepage = "https://gitlab.com/stratux/stratux-websockets";
  description = "A library for using websockets with stratux";
  license = stdenv.lib.licenses.bsd3;
}
